package com.example.adventofcode.api;

import com.example.adventofcode.models.UserResponse;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface GetUserResponseService{
    @GET("userResponse/user/resolved/{resolved}")
    Call<List<UserResponse>>  findAnswerCodeByResolvedAndAccountId(@Path("resolved") Boolean resolved, @Header("Authorization") String token);

}
