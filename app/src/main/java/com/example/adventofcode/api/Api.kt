package com.example.adventofcode.api

import com.example.adventofcode.models.DefaultResponse
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.Field
import retrofit2.http.Headers
import retrofit2.http.POST
import retrofit2.http.FormUrlEncoded
import java.util.*

interface Api {
    @FormUrlEncoded
    @POST("register")
    fun register(
        @Field("email") email: String,
        @Field("username") username: String,
        @Field("password") password: String,
        @Field("birthdate") birthdate: Date
    ):Call<DefaultResponse>


    /*@Headers("Content-Type:application/json")
    @POST("/login")
    fun login(email: String ,
              password: String): Call<ResponseBody>*/

}