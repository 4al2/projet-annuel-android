package com.example.adventofcode.api

import com.example.adventofcode.models.LoginDTO
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.Headers
import retrofit2.http.POST

interface AuthService {

        @POST("auth/login")
        fun login(@Body loginDTO: LoginDTO): Call<ResponseBody>

    }