package com.example.adventofcode.api

import com.example.adventofcode.models.LoginDTO
import com.example.adventofcode.models.UserResponse
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.*

interface ApiService {

    @POST("auth/login")
    fun login(@Body loginDTO: LoginDTO): Call<ResponseBody>


    @GET("userResponse/user/resolved/{resolved}")
    fun findAnswerCodeByResolvedAndAccountId(@Path("resolved") resolved: Boolean?, @Header("Authorization") token: String
    ): Call<List<UserResponse>>

}