package com.example.adventofcode.api

import android.content.Context
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.*


object ApiClient {

    private const val BASE_URL: String = "http://10.0.2.2:9090/api/"
    private val AUTH =
        "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiIyMTg2OGFkYi00ZGRiLTQ4MGItOGQyMS0xMjBkYzk1YjM2YWIiLCJhdXRoIjoiUk9MRV9VU0VSIiwiZXhwIjoxNjI0MDI1NjgxfQ.Xy36qaYw74wyoR_Mvhkch_BfZtyoRH_8n3tNpAz7gw0QeSj3gAOnjTWyaI9pE5q_PsWZR5gMRkUgvuoy6F67Vg"
    private val gson: Gson by lazy {
        GsonBuilder().setLenient().create()
    }
    private lateinit var apiService: ApiService

    /*fun getApiService(context: Context): ApiService {

        if (!::apiService.isInitialized) {

            val retrofit: Retrofit by lazy {
                Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .client(httpClient(context))
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .build()
            }

            apiService = retrofit.create(ApiService::class.java)

        }
        return apiService

    }*/

    /*private fun httpClient(context: Context): OkHttpClient {

        return OkHttpClient.Builder()
            .addInterceptor(AuthInterceptor(context))
            .build()
    }*/


    private val httpClient: OkHttpClient by lazy {
        OkHttpClient.Builder()
            .addInterceptor { chain ->
                val original = chain.request()
                val requestBuilder = original.newBuilder()
                    .method(original.method(), original.body())

                val request = requestBuilder.build()
                chain.proceed(request)
            }.build()

    }


    private val retrofit: Retrofit by lazy {
        Retrofit.Builder()
            .baseUrl(BASE_URL)
            .client(httpClient)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build()
    }

    val authService: AuthService by lazy {
        retrofit.create(AuthService::class.java)
    }

    val getUserResponseService: GetUserResponseService =
        retrofit.create(GetUserResponseService::class.java)
}



