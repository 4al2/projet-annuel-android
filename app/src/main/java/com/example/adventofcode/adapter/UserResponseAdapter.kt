package com.example.adventofcode.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.adventofcode.R
import com.example.adventofcode.models.UserResponse

internal  class UserResponseAdapter (private var userResponseList: List<UserResponse>):
    RecyclerView.Adapter<UserResponseAdapter.MyViewHolder>(){
    internal inner class MyViewHolder(view: View): RecyclerView.ViewHolder(view){
        var userResponseView: TextView = view.findViewById(R.id.userResponseView)
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): UserResponseAdapter.MyViewHolder {
        var itemView = LayoutInflater.from(parent.context).inflate(R.layout.user_response, parent, false)
        return MyViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: UserResponseAdapter.MyViewHolder, position: Int) {
        val item = userResponseList[position]
        holder.userResponseView.text = item.toString()
    }

    override fun getItemCount(): Int {
        return userResponseList.size
    }


}