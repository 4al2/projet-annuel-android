package com.example.adventofcode.models

data class LoginDTO (
     val username: String ,
     val password: String

)