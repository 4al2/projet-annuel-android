package com.example.adventofcode.models

import java.util.*

data class User(
                var id: String = "",
                var username: String = "",
                var email: String = "",
                var birthdate: Date? = null,
                var password: String = "")

