package com.example.adventofcode.models

import java.lang.StringBuilder

data class DefaultResponse(val error: Boolean, val message: String)
