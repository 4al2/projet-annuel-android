package com.example.adventofcode.models

import java.util.*

data class UserResponse (
    var  code: String? = "" ,
    var resolved: Boolean? = false,
    var resolutionDate: Date? = null,
    var CodeQuality: String? = "",
    var idStatement: String? = "",
    var language: String? = ""


)